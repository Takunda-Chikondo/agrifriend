#!/bin/sh
cd ..
source bin/activate
export DJANGO_SETTINGS_MODULE=agrifriend.settings.production
cd agrifriend
git pull origin master
pip install -r requirements/production.txt
python manage.py collectstatic --noinput
python manage.py migrate
sudo supervisorctl restart agrifriend
exit