# Generated by Django 2.2 on 2020-05-25 21:49

from django.db import migrations
import mapbox_location_field.models


class Migration(migrations.Migration):

    dependencies = [
        ('agrifriend', '0033_auto_20200525_2348'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='location',
            field=mapbox_location_field.models.LocationField(default=(-19.21526180699169, 30.450138345933965), map_attrs={'center': [30.01667, -19.01667], 'marker_color': 'red', 'zoom': 5}),
        ),
    ]
