# Generated by Django 2.2 on 2020-02-29 17:02

from django.db import migrations
import mapbox_location_field.models


class Migration(migrations.Migration):

    dependencies = [
        ('agrifriend', '0021_auto_20200229_0217'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='location',
            field=mapbox_location_field.models.LocationField(map_attrs={'center': [30.01667, -19.01667], 'marker_color': 'blue', 'zoom': 5}),
        ),
    ]
