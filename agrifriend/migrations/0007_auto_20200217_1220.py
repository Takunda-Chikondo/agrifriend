# Generated by Django 2.2 on 2020-02-17 10:20

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('agrifriend', '0006_cropschedule_is_produced'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='CropProduction',
            name='is_produced',
        ),
        migrations.AlterField(
            model_name='CropProduction',
            name='farmer',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='agrifriend.Profile'),
        ),
        migrations.AlterField(
            model_name='profile',
            name='user',
            field=models.OneToOneField(default=0, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
