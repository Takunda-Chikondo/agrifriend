# Generated by Django 2.2 on 2020-02-29 00:00

from django.db import migrations
import mapbox_location_field.models


class Migration(migrations.Migration):

    dependencies = [
        ('agrifriend', '0018_auto_20200229_0158'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='location',
            field=mapbox_location_field.models.LocationField(default=(-19.01667, 30.01667), map_attrs={'center': [-19.01667, 30.01667], 'marker_color': 'blue'}),
        ),
    ]
