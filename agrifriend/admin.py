from django.contrib import admin
from django.template.response import TemplateResponse
from django.urls import path
from agrifriend.models import Profile, Crop, CropProduction, Animal, AnimalProduction
from agrifriend.admin_helpers import export_to_excel

class ProfileAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'phone_number', 'nearest_growth_point', 'farm_size')
    list_filter = ['nearest_growth_point','farm_size']
    search_fields = ['first_name','last_name']


class AnimalProductionAdmin(admin.ModelAdmin):
        list_display = ('farmer', 'animal_type', 'quantity_per_week', 'market_ready_date')
        list_filter = ['animal_type', 'market_ready_date']
        search_fields = ['first_name','last_name']
        actions = [export_to_excel]


class CropScheduleAdmin(admin.ModelAdmin):
        list_display = ('farmer', 'crop_type', 'quantity_per_week', 'market_ready_date')
        list_filter = ['crop_type', 'market_ready_date']
        search_fields = ['first_name','last_name']
        actions = [export_to_excel]


admin.site.register(CropProduction, CropScheduleAdmin)
admin.site.register(Profile, ProfileAdmin)
admin.site.register(AnimalProduction, AnimalProductionAdmin)
admin.site.register(Crop)
admin.site.register(Animal)
admin.site.site_header = ("Agrifriend Administration")
