from django.db import models
from django.contrib.auth import get_user_model
from django.utils import timezone
from django.dispatch import receiver
from django.conf import settings
from mapbox_location_field.models import LocationField
from django.db.models.signals import post_save

import datetime
# using the custom user model instead of contrib.auth user
User = get_user_model()

class Profile(models.Model):
    GENDER_DEFAULT = 'select gender'
    MALE = 'male'
    FEMALE = 'female'

    GENDER_CHOICES = (
        (GENDER_DEFAULT, 'select gender'),
        (MALE, 'male'),
        (FEMALE, 'female'),
    )

    SOIL_TYPE_DEFAULT = 'select soil type'
    RED_CLAY = 'red clay'
    BLACK_CLAY = 'black clay'
    SAND = 'sand'
    SILT = 'silt'
    LOAM = 'loam'
    CLAY_LOAM = 'clay loam'
    SANDY_LOAM = 'sandy loam'

    SOIL_TYPE_CHOICES = (
        (SOIL_TYPE_DEFAULT, 'select soil type'),
        (RED_CLAY, 'red clay'),
        (BLACK_CLAY, 'black clay'),
        (SAND, 'sand'),
        (SILT, 'silt'),
        (LOAM, 'loam'),
        (CLAY_LOAM, 'clay loam'),
        (SANDY_LOAM, 'sandy loam'),
    )

    FARMING_VENTURE_DEFAULT = 'select a farming venture'
    CROP_PRODUCTION = 'crop production'
    HORTICULTURE = 'horticulture'
    FLORICULTURE = 'floriculture'
    LIVESTOCK = 'livestock'
    POULTRY = 'poultry'
    CLAY_LOAM = 'clay loam'
    SANDY_LOAM = 'sandy loam'

    FARMING_VENTURE_CHOICES = (
        (FARMING_VENTURE_DEFAULT, 'select a farming venture'),
        (CROP_PRODUCTION, 'crop production'),
        (HORTICULTURE, 'horticulture'),
        (FLORICULTURE, 'floriculture'),
        (LIVESTOCK, 'livestock'),
        (POULTRY, 'poultry'),
    )

    MECHANISATION_LEVEL_DEFAULT = 'not selected'
    DRAUGHT = 'draught'
    TRACTOR = 'tractor'

    MECHANISATION_LEVEL_CHOICES = (
        (MECHANISATION_LEVEL_DEFAULT, 'select mechanisation level'),
        (DRAUGHT, 'draught'),
        (TRACTOR, 'tractor'),
    )

    IRRIGATION_LEVEL_DEFAULT = 'not selected'
    RAIN_FED = 'rain fed'
    DRIP = 'drip'
    OVERHEAD = 'overhead'
    FLOOD = 'flood'

    IRRIGATION_LEVEL_CHOICES = (
        (IRRIGATION_LEVEL_DEFAULT, 'select irrigation type'),
        (RAIN_FED, 'rain fed'),
        (DRIP, 'drip'),
        (OVERHEAD, 'overhead'),
        (FLOOD, 'flood'), 
    )

    SOURCE_OF_IRRIGATION_WATER_DEFAULT = 'not selected'
    WELL = 'rain fed'
    BOREHOLE = 'borehole'
    DAM = 'dam'
    RIVER = 'river'
    MAKESHIFT_WEIR = 'make shift weir'

    SOURCE_OF_IRRIGATION_WATER_CHOICES = (
        (SOURCE_OF_IRRIGATION_WATER_DEFAULT, 'select source'),
        (WELL, 'rain fed'),
        (BOREHOLE, 'borehole'),
        (DAM, 'dam'),
        (RIVER, 'river'),
        (MAKESHIFT_WEIR, 'make shift weir') 
    )

    LABOUR_STRUCTURE_DEFAULT = 'not selected'
    FAMILY = 'family'
    SMALL = '1-10'
    MEDIUM = '10-50'
    LARGE = '50-10'

    LABOUR_STRUCTURE_CHOICES = (
        (LABOUR_STRUCTURE_DEFAULT, 'select source'),
        (FAMILY, 'family'),
        (SMALL, '1-10'),
        (MEDIUM, '10-50'),
        (LARGE, '50-10'),
    )

    user = models.OneToOneField(User, on_delete=models.CASCADE, default=0)
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    gender = models.CharField(max_length=14, choices=GENDER_CHOICES, default=GENDER_DEFAULT)
    phone_number = models.CharField(max_length=200)
    profile_picture = models.ImageField(upload_to='profile_pictures', null=True, blank=True)
    nearest_growth_point = models.CharField(max_length=200)
    location = LocationField(map_attrs={"center": [30.01667,-19.01667], "zoom": 5, "marker_color": "red"}, default=(30.01667,-19.01667))
    farm_size = models.IntegerField(default=0)
    total_arable_land = models.IntegerField(default=0)
    total_irrigable_land = models.IntegerField(default=0)
    
    soil_type = models.CharField(max_length=50, choices=SOIL_TYPE_CHOICES, default=SOIL_TYPE_DEFAULT)
    agro_ecological_region = models.CharField(max_length=200, default='none')
    farming_venture = models.CharField(max_length=50, choices=FARMING_VENTURE_CHOICES, default=FARMING_VENTURE_DEFAULT)
    mechanization_level = models.CharField(max_length=50, choices=MECHANISATION_LEVEL_CHOICES, default=MECHANISATION_LEVEL_DEFAULT)
    irrigation_facilities = models.CharField(max_length=50, choices=IRRIGATION_LEVEL_CHOICES, default=IRRIGATION_LEVEL_DEFAULT)
    source_of_irrigation_water = models.CharField(max_length=50, choices=SOURCE_OF_IRRIGATION_WATER_CHOICES, default=SOURCE_OF_IRRIGATION_WATER_DEFAULT)
    seasonality = models.CharField(max_length=200, default='none') 
    financial_source = models.CharField(max_length=200, default='self')
    labour_structure = models.CharField(max_length=50, choices=LABOUR_STRUCTURE_CHOICES, default=LABOUR_STRUCTURE_DEFAULT)

    def __str__(self):
        return self.first_name + " " + self.last_name

@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()

class CropProduction(models.Model):
    # one farmer can have many crop schedules
    farmer = models.ForeignKey(Profile, on_delete=models.CASCADE)
    crop_type = models.CharField(max_length=200)
    quantity_per_week = models.IntegerField(default=0)
    market_ready_date = models.DateField(default=datetime.date.today)
    crop_image = models.ImageField(upload_to='crops', null=True)
    
    class Meta:
        verbose_name_plural = "Crop Production"

    def __str__(self):
        return self.farmer.first_name + " " + self.farmer.last_name


class AnimalProduction(models.Model):
    # one farmer can have many different animals produced
    farmer = models.ForeignKey(Profile, on_delete=models.CASCADE)
    animal_type = models.CharField(max_length=200)
    quantity_per_week = models.IntegerField(default=0)
    market_ready_date = models.DateField(default=datetime.date.today)
    animal_image = models.ImageField(upload_to='animals', null=True)

    class Meta:
        verbose_name_plural = "Animal Production"

    def __str__(self):
        return self.farmer.first_name + " " + self.farmer.last_name


class Vendor_profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, default=0)
    account_name = models.CharField(max_length=200)
    province = models.CharField(max_length=200)
    area = models.CharField(max_length=200)
    crops_needed = models.CharField(max_length=200)
    intervals = models.CharField(max_length=200)

class Crop(models.Model):
    crop_name = models.CharField(max_length=200)

    def __str__(self):
        return self.crop_name

class Animal(models.Model):
    animal_name = models.CharField(max_length=200)

    def __str__(self):
        return self.animal_name