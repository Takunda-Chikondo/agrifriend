from django.http import HttpResponse
from django.core.mail import send_mail, BadHeaderError
from django.template import loader
from django.contrib import messages
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from .forms import ContactForm
from . models import Profile
import pdb

context = {}
def index(request):
    template = loader.get_template('index.html')
    return HttpResponse(template.render({'context': context}, request))

def contact(request):
    template = loader.get_template('contact.html')
    return HttpResponse(template.render({'context': context}, request))

def contactView(request):
    template = loader.get_template('contact.html')

    if request.method == 'GET':
        form = ContactForm()
    else:
        form = ContactForm(request.POST)
        if form.is_valid():
            subject = form.cleaned_data['subject']
            from_email = form.cleaned_data['from_email']
            message = form.cleaned_data['message']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            company = form.cleaned_data['company']
            message = message + "\n\n" + "regards" + "\n\n" + first_name + " " + last_name + "\n\n" + company
            try:
                send_mail(subject, message, from_email, ['admin@example.com'])
                messages.success(request, 'Message sent successfully',extra_tags='alert')

            except BadHeaderError:
                return HttpResponse('Invalid header found.')

    return HttpResponse(template.render({'form': form }, request))

@login_required
def visualizationView(request):
    template = loader.get_template('visualization.html')
    context['farms'] = Profile.objects.all()
    return HttpResponse(template.render({'context': context}, request))
