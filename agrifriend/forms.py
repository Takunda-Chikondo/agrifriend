from django import forms

class ContactForm(forms.Form):
    first_name = forms.CharField(required=True)
    last_name = forms.CharField(required=True)
    from_email = forms.EmailField(label='Email', required=True)
    subject = forms.CharField(required=True)
    company = forms.CharField(required=False)
    message = forms.CharField(widget=forms.Textarea, required=True)
