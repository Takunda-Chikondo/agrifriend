import xlwt
import csv
from django.http import HttpResponse
from agrifriend.models import Profile, CropProduction, AnimalProduction

def export_to_excel(modeladmin, request, queryset):
        response = HttpResponse(content_type='text/csv')

        if "animalproduction" in request.path:
                response['Content-Disposition'] = 'attachment; filename="animal_prod.csv"'
                writer = csv.writer(response)
                writer.writerow(['Farmer', 'Animal', 'Quantity per Week', 'Market Ready Date'])

                animals = AnimalProduction.objects.all().values_list('farmer','animal_type', 'quantity_per_week', 'market_ready_date')
                for animal in animals:
                        farmer = Profile.objects.get(pk=animal[0])
                        lst = list(animal)
                        lst[0] = farmer.first_name +" "+ farmer.last_name
                        t = tuple(lst)
                        writer.writerow(t)

        elif "cropproduction" in request.path:
                response['Content-Disposition'] = 'attachment; filename="crop_prod.csv"'
                writer = csv.writer(response)
                writer.writerow(['Farmer', 'Crop', 'Quantity per Week', 'Market Ready Date'])

                crops = CropProduction.objects.all().values_list('farmer','crop_type', 'quantity_per_week', 'market_ready_date')
                for crop in crops:
                        farmer = Profile.objects.get(pk=crop[0])
                        lst = list(crop)
                        lst[0] = farmer.first_name +" "+ farmer.last_name
                        t = tuple(lst)
                        writer.writerow(t)
        return response
