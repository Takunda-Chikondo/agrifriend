--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.19
-- Dumped by pg_dump version 9.5.19

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: agrifriend_animal; Type: TABLE; Schema: public; Owner: postgres_admin
--

CREATE TABLE public.agrifriend_animal (
    id integer NOT NULL,
    animal_name character varying(200) NOT NULL
);


ALTER TABLE public.agrifriend_animal OWNER TO postgres_admin;

--
-- Name: agrifriend_animal_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres_admin
--

CREATE SEQUENCE public.agrifriend_animal_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agrifriend_animal_id_seq OWNER TO postgres_admin;

--
-- Name: agrifriend_animal_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres_admin
--

ALTER SEQUENCE public.agrifriend_animal_id_seq OWNED BY public.agrifriend_animal.id;


--
-- Name: agrifriend_animalproduction; Type: TABLE; Schema: public; Owner: postgres_admin
--

CREATE TABLE public.agrifriend_animalproduction (
    id integer NOT NULL,
    animal_type character varying(200) NOT NULL,
    quantity_per_week integer NOT NULL,
    market_ready_date date NOT NULL,
    farmer_id integer NOT NULL,
    animal_image character varying(100)
);


ALTER TABLE public.agrifriend_animalproduction OWNER TO postgres_admin;

--
-- Name: agrifriend_animalproduction_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres_admin
--

CREATE SEQUENCE public.agrifriend_animalproduction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agrifriend_animalproduction_id_seq OWNER TO postgres_admin;

--
-- Name: agrifriend_animalproduction_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres_admin
--

ALTER SEQUENCE public.agrifriend_animalproduction_id_seq OWNED BY public.agrifriend_animalproduction.id;


--
-- Name: agrifriend_crop; Type: TABLE; Schema: public; Owner: postgres_admin
--

CREATE TABLE public.agrifriend_crop (
    id integer NOT NULL,
    crop_name character varying(200) NOT NULL
);


ALTER TABLE public.agrifriend_crop OWNER TO postgres_admin;

--
-- Name: agrifriend_cropproduction; Type: TABLE; Schema: public; Owner: postgres_admin
--

CREATE TABLE public.agrifriend_cropproduction (
    id integer NOT NULL,
    crop_type character varying(200) NOT NULL,
    quantity_per_week integer NOT NULL,
    market_ready_date date NOT NULL,
    farmer_id integer NOT NULL,
    crop_image character varying(100)
);


ALTER TABLE public.agrifriend_cropproduction OWNER TO postgres_admin;

--
-- Name: agrifriend_crop_schedule_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres_admin
--

CREATE SEQUENCE public.agrifriend_crop_schedule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agrifriend_crop_schedule_id_seq OWNER TO postgres_admin;

--
-- Name: agrifriend_crop_schedule_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres_admin
--

ALTER SEQUENCE public.agrifriend_crop_schedule_id_seq OWNED BY public.agrifriend_cropproduction.id;


--
-- Name: agrifriend_crop_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres_admin
--

CREATE SEQUENCE public.agrifriend_crop_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agrifriend_crop_type_id_seq OWNER TO postgres_admin;

--
-- Name: agrifriend_crop_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres_admin
--

ALTER SEQUENCE public.agrifriend_crop_type_id_seq OWNED BY public.agrifriend_crop.id;


--
-- Name: agrifriend_profile; Type: TABLE; Schema: public; Owner: postgres_admin
--

CREATE TABLE public.agrifriend_profile (
    id integer NOT NULL,
    first_name character varying(200) NOT NULL,
    last_name character varying(200) NOT NULL,
    phone_number character varying(200) NOT NULL,
    nearest_growth_point character varying(200) NOT NULL,
    farm_size integer NOT NULL,
    user_id integer NOT NULL,
    location character varying(63) NOT NULL,
    gender character varying(14) NOT NULL,
    total_arable_land integer NOT NULL,
    total_irrigable_land integer NOT NULL,
    agro_ecological_region character varying(200) NOT NULL,
    soil_type character varying(50) NOT NULL,
    farming_venture character varying(50) NOT NULL,
    mechanization_level character varying(50) NOT NULL,
    irrigation_facilities character varying(50) NOT NULL,
    source_of_irrigation_water character varying(50) NOT NULL,
    financial_source character varying(200) NOT NULL,
    labour_structure character varying(50) NOT NULL,
    seasonality character varying(200) NOT NULL,
    profile_picture character varying(100)
);


ALTER TABLE public.agrifriend_profile OWNER TO postgres_admin;

--
-- Name: agrifriend_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres_admin
--

CREATE SEQUENCE public.agrifriend_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agrifriend_profile_id_seq OWNER TO postgres_admin;

--
-- Name: agrifriend_profile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres_admin
--

ALTER SEQUENCE public.agrifriend_profile_id_seq OWNED BY public.agrifriend_profile.id;


--
-- Name: agrifriend_vendor_profile; Type: TABLE; Schema: public; Owner: postgres_admin
--

CREATE TABLE public.agrifriend_vendor_profile (
    id integer NOT NULL,
    account_name character varying(200) NOT NULL,
    province character varying(200) NOT NULL,
    area character varying(200) NOT NULL,
    crops_needed character varying(200) NOT NULL,
    intervals character varying(200) NOT NULL,
    user_id integer NOT NULL
);


ALTER TABLE public.agrifriend_vendor_profile OWNER TO postgres_admin;

--
-- Name: agrifriend_vendor_profile_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres_admin
--

CREATE SEQUENCE public.agrifriend_vendor_profile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.agrifriend_vendor_profile_id_seq OWNER TO postgres_admin;

--
-- Name: agrifriend_vendor_profile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres_admin
--

ALTER SEQUENCE public.agrifriend_vendor_profile_id_seq OWNED BY public.agrifriend_vendor_profile.id;


--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres_admin
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(150) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO postgres_admin;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres_admin
--

CREATE SEQUENCE public.auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO postgres_admin;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres_admin
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres_admin
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO postgres_admin;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres_admin
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO postgres_admin;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres_admin
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres_admin
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO postgres_admin;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres_admin
--

CREATE SEQUENCE public.auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO postgres_admin;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres_admin
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres_admin
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO postgres_admin;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres_admin
--

CREATE SEQUENCE public.django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO postgres_admin;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres_admin
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres_admin
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO postgres_admin;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres_admin
--

CREATE SEQUENCE public.django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO postgres_admin;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres_admin
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres_admin
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO postgres_admin;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres_admin
--

CREATE SEQUENCE public.django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO postgres_admin;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres_admin
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres_admin
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO postgres_admin;

--
-- Name: useraccounts_user; Type: TABLE; Schema: public; Owner: postgres_admin
--

CREATE TABLE public.useraccounts_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL,
    email character varying(254) NOT NULL,
    is_farmer boolean NOT NULL,
    is_vendor boolean NOT NULL
);


ALTER TABLE public.useraccounts_user OWNER TO postgres_admin;

--
-- Name: useraccounts_user_groups; Type: TABLE; Schema: public; Owner: postgres_admin
--

CREATE TABLE public.useraccounts_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.useraccounts_user_groups OWNER TO postgres_admin;

--
-- Name: useraccounts_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres_admin
--

CREATE SEQUENCE public.useraccounts_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.useraccounts_user_groups_id_seq OWNER TO postgres_admin;

--
-- Name: useraccounts_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres_admin
--

ALTER SEQUENCE public.useraccounts_user_groups_id_seq OWNED BY public.useraccounts_user_groups.id;


--
-- Name: useraccounts_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres_admin
--

CREATE SEQUENCE public.useraccounts_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.useraccounts_user_id_seq OWNER TO postgres_admin;

--
-- Name: useraccounts_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres_admin
--

ALTER SEQUENCE public.useraccounts_user_id_seq OWNED BY public.useraccounts_user.id;


--
-- Name: useraccounts_user_user_permissions; Type: TABLE; Schema: public; Owner: postgres_admin
--

CREATE TABLE public.useraccounts_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.useraccounts_user_user_permissions OWNER TO postgres_admin;

--
-- Name: useraccounts_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres_admin
--

CREATE SEQUENCE public.useraccounts_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.useraccounts_user_user_permissions_id_seq OWNER TO postgres_admin;

--
-- Name: useraccounts_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres_admin
--

ALTER SEQUENCE public.useraccounts_user_user_permissions_id_seq OWNED BY public.useraccounts_user_user_permissions.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.agrifriend_animal ALTER COLUMN id SET DEFAULT nextval('public.agrifriend_animal_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.agrifriend_animalproduction ALTER COLUMN id SET DEFAULT nextval('public.agrifriend_animalproduction_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.agrifriend_crop ALTER COLUMN id SET DEFAULT nextval('public.agrifriend_crop_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.agrifriend_cropproduction ALTER COLUMN id SET DEFAULT nextval('public.agrifriend_crop_schedule_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.agrifriend_profile ALTER COLUMN id SET DEFAULT nextval('public.agrifriend_profile_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.agrifriend_vendor_profile ALTER COLUMN id SET DEFAULT nextval('public.agrifriend_vendor_profile_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.useraccounts_user ALTER COLUMN id SET DEFAULT nextval('public.useraccounts_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.useraccounts_user_groups ALTER COLUMN id SET DEFAULT nextval('public.useraccounts_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.useraccounts_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.useraccounts_user_user_permissions_id_seq'::regclass);


--
-- Data for Name: agrifriend_animal; Type: TABLE DATA; Schema: public; Owner: postgres_admin
--

COPY public.agrifriend_animal (id, animal_name) FROM stdin;
1	Goats
2	Sheep
3	Fish
4	Broiler
5	Roadrunner
6	Eggs
7	Pig
8	Goat
9	Sheep
10	Guinea Fowl
11	Turkey
12	Cattle
13	Dairy
\.


--
-- Name: agrifriend_animal_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres_admin
--

SELECT pg_catalog.setval('public.agrifriend_animal_id_seq', 13, true);


--
-- Data for Name: agrifriend_animalproduction; Type: TABLE DATA; Schema: public; Owner: postgres_admin
--

COPY public.agrifriend_animalproduction (id, animal_type, quantity_per_week, market_ready_date, farmer_id, animal_image) FROM stdin;
\.


--
-- Name: agrifriend_animalproduction_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres_admin
--

SELECT pg_catalog.setval('public.agrifriend_animalproduction_id_seq', 2, true);


--
-- Data for Name: agrifriend_crop; Type: TABLE DATA; Schema: public; Owner: postgres_admin
--

COPY public.agrifriend_crop (id, crop_name) FROM stdin;
1	Covo
2	Rape
3	Maize
4	Irish Potato
5	Sweet Potato
6	Carrots
7	Butternuts
8	Tomato
9	Green Pepper
10	Yellow Pepper
11	Red pepper
12	Cucumber
13	English Cucumber
14	Onion
15	Shallots
16	Garlic
17	Ginger
18	Brocolli
19	Cauliflower
20	Lettuce
21	Cabbage
22	Green Beans
23	Dried Beans
24	Peas
25	Okra
26	Baby Marrow
27	Green Mealies
28	Water Melon
29	Banana
30	Mango
31	Orange
32	Lemon
33	Apple
34	Peach
35	PineApple
36	Marcademia
37	Groundnut
38	Roundnut
39	Cowpeas
40	Rapoko
41	Sorghum
42	Tobacco
43	Cotton
44	Soya Bean
45	Roses
46	Cutflowers
47	Dried Vegetables
48	Sugarcane
49	Pumpkin
50	Sunflower
\.


--
-- Name: agrifriend_crop_schedule_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres_admin
--

SELECT pg_catalog.setval('public.agrifriend_crop_schedule_id_seq', 5, true);


--
-- Name: agrifriend_crop_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres_admin
--

SELECT pg_catalog.setval('public.agrifriend_crop_type_id_seq', 50, true);


--
-- Data for Name: agrifriend_cropproduction; Type: TABLE DATA; Schema: public; Owner: postgres_admin
--

COPY public.agrifriend_cropproduction (id, crop_type, quantity_per_week, market_ready_date, farmer_id, crop_image) FROM stdin;
4	Cauliflower	50	2020-06-22	12	crops/IMG_20200616_174239.jpg
5	Onion	10	2020-08-30	14	crops/IMG_20200614_1531421.jpg
\.


--
-- Data for Name: agrifriend_profile; Type: TABLE DATA; Schema: public; Owner: postgres_admin
--

COPY public.agrifriend_profile (id, first_name, last_name, phone_number, nearest_growth_point, farm_size, user_id, location, gender, total_arable_land, total_irrigable_land, agro_ecological_region, soil_type, farming_venture, mechanization_level, irrigation_facilities, source_of_irrigation_water, financial_source, labour_structure, seasonality, profile_picture) FROM stdin;
12	Rachel	Matangira	0772846089	Whitecliff	1	41	30.01667,-19.01667	select gender	1	1	2	sandy loam	horticulture	draught	drip	river	self	1-10	All year round	
6	Chipo	Masuka	0776375475	Furtherstone	400	36	30.01667,-19.01667	select gender	0	0	none	select soil type	select a farming venture	not selected	not selected	not selected	self	not selected	none	\N
7	Itai	Masuka	0000000	Harvard	0	16	-19.01667,30.01667	male	0	0	none	select soil type	select a farming venture	not selected	not selected	not selected	self	not selected	none	
8	Tapera	Munyimani	00263772488824	Banket	10	37	30.01667,-19.01667	select gender	0	0	none	select soil type	select a farming venture	not selected	not selected	not selected	self	not selected	none	\N
13	Simbarashe	Mudawapi	+263772776159	Bindura	1	42	30.01667,-19.01667	select gender	0	0	none	select soil type	select a farming venture	not selected	not selected	not selected	self	not selected	none	
5	Takunda	Chikondo	+263776192859	buhera	125	1	31.110358165113496,-17.78104978084859	male	0	0	none	select soil type	select a farming venture	not selected	not selected	not selected	self	not selected	none	profile_pictures/profile.png
9	Jeopardy	Chineka	+263773818687	Beatrice	465	38	30.01667,-19.01667	select gender	0	0	none	select soil type	select a farming venture	not selected	not selected	not selected	self	not selected	none	
10	Clarence	Mashavave	712626828	Nyabira	53	39	30.01667,-19.01667	select gender	0	0	none	select soil type	select a farming venture	not selected	not selected	not selected	self	not selected	none	
11	Kudakwashe	Masiiwa	0779854307	Mahusekwa	5	40	30.01667,-19.01667	select gender	0	0	none	select soil type	select a farming venture	not selected	not selected	not selected	self	not selected	none	
14	collins	chizhande	+263773941401	Harare	1770	43	30.01667,-19.01667	male	770	200	region 2B	red clay	crop production	tractor	overhead	borehole	self	50-10	none	profile_pictures/IMG_20200121_111315.jpg
\.


--
-- Name: agrifriend_profile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres_admin
--

SELECT pg_catalog.setval('public.agrifriend_profile_id_seq', 14, true);


--
-- Data for Name: agrifriend_vendor_profile; Type: TABLE DATA; Schema: public; Owner: postgres_admin
--

COPY public.agrifriend_vendor_profile (id, account_name, province, area, crops_needed, intervals, user_id) FROM stdin;
\.


--
-- Name: agrifriend_vendor_profile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres_admin
--

SELECT pg_catalog.setval('public.agrifriend_vendor_profile_id_seq', 1, false);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: postgres_admin
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres_admin
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: postgres_admin
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres_admin
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: postgres_admin
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can view log entry	1	view_logentry
5	Can add permission	2	add_permission
6	Can change permission	2	change_permission
7	Can delete permission	2	delete_permission
8	Can view permission	2	view_permission
9	Can add group	3	add_group
10	Can change group	3	change_group
11	Can delete group	3	delete_group
12	Can view group	3	view_group
13	Can add content type	4	add_contenttype
14	Can change content type	4	change_contenttype
15	Can delete content type	4	delete_contenttype
16	Can view content type	4	view_contenttype
17	Can add session	5	add_session
18	Can change session	5	change_session
19	Can delete session	5	delete_session
20	Can view session	5	view_session
21	Can add profile	8	add_profile
22	Can change profile	8	change_profile
23	Can delete profile	8	delete_profile
24	Can view profile	8	view_profile
25	Can add crop production	7	add_cropproduction
26	Can change crop production	7	change_cropproduction
27	Can delete crop production	7	delete_cropproduction
28	Can view crop production	7	view_cropproduction
29	Can add animal	9	add_animal
30	Can change animal	9	change_animal
31	Can delete animal	9	delete_animal
32	Can view animal	9	view_animal
33	Can add vendor_profile	11	add_vendor_profile
34	Can change vendor_profile	11	change_vendor_profile
35	Can delete vendor_profile	11	delete_vendor_profile
36	Can view vendor_profile	11	view_vendor_profile
37	Can add animal production	10	add_animalproduction
38	Can change animal production	10	change_animalproduction
39	Can delete animal production	10	delete_animalproduction
40	Can view animal production	10	view_animalproduction
41	Can add crop	6	add_crop
42	Can change crop	6	change_crop
43	Can delete crop	6	delete_crop
44	Can view crop	6	view_crop
45	Can add user	12	add_user
46	Can change user	12	change_user
47	Can delete user	12	delete_user
48	Can view user	12	view_user
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres_admin
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 48, true);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: postgres_admin
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2020-02-25 20:02:39.358474+00	3	a@a.com	3		12	1
2	2020-02-25 20:02:39.362065+00	8	a@p.com	3		12	1
3	2020-02-25 20:02:39.363658+00	7	a@t.com	3		12	1
4	2020-02-25 20:02:39.3651+00	6	a@w.com	3		12	1
5	2020-02-25 20:02:39.366603+00	4	q@e.com	3		12	1
6	2020-02-25 20:02:39.368123+00	5	q@s.com	3		12	1
7	2020-02-25 20:02:39.369781+00	2	tkchikondo@outlook.com	3		12	1
8	2020-02-25 20:27:27.385448+00	9	tkchikondo@outlook.com	3		12	1
9	2020-02-26 20:11:35.247804+00	11	leyih24526@jszmail.com	3		12	1
10	2020-02-26 20:11:35.24985+00	12	masukamj@gmail.com	3		12	1
11	2020-02-26 20:11:35.250867+00	10	tkchikondo@outlook.com	3		12	1
12	2020-02-26 22:09:53.746844+00	1	Covo	1	[{"added": {}}]	6	1
13	2020-02-26 22:09:59.935461+00	2	Rape	1	[{"added": {}}]	6	1
14	2020-02-26 22:10:25.641006+00	1	Goats	1	[{"added": {}}]	9	1
15	2020-02-26 22:10:34.546772+00	2	Sheep	1	[{"added": {}}]	9	1
16	2020-02-26 22:16:09.896969+00	14	bodoy98079@nuevomail.com	3		12	1
17	2020-02-26 22:16:09.900026+00	15	jigeh82602@p5mail.com	3		12	1
18	2020-02-26 22:16:09.901101+00	13	tkchikondo@outlook.com	3		12	1
19	2020-02-26 22:17:31.015+00	16	engmasuka@ulitmeaccolade.co.zw	1	[{"added": {}}]	12	1
20	2020-02-26 22:17:49.795376+00	16	engmasuka@ulitmeaccolade.co.zw	2	[{"changed": {"fields": ["first_name", "last_name", "is_superuser"]}}]	12	1
21	2020-02-26 22:18:08.631468+00	16	engmasuka@ulitmeaccolade.co.zw	2	[{"changed": {"fields": ["is_staff"]}}]	12	1
22	2020-02-27 00:52:18.417466+00	19	bouchee@claimyourprize.life	3		12	1
23	2020-02-27 00:52:18.420258+00	18	jigeh82602@p5mail.com	3		12	1
24	2020-02-27 00:52:18.421243+00	17	tkchikondo@outlook.com	3		12	1
25	2020-02-27 01:04:42.26436+00	20	tkchikondo@outlook.com	3		12	1
26	2020-02-27 01:09:46.532667+00	21	tkchikondo@outlook.com	3		12	1
27	2020-02-27 01:27:54.09514+00	23	gaxato4968@smlmail.net	3		12	1
28	2020-02-27 01:27:54.097586+00	22	tkchikondo@outlook.com	3		12	1
29	2020-02-27 01:40:25.082345+00	24	gaxato4968@smlmail.net	3		12	1
30	2020-02-27 01:48:27.519263+00	25	tkchikondo@outlook.com	3		12	1
31	2020-02-27 01:50:03.634756+00	26	gaxato4968@smlmail.net	3		12	1
32	2020-03-09 07:18:48.880396+00	3	Maize	1	[{"added": {}}]	6	16
33	2020-03-09 07:19:24.875077+00	4	Irish Potato	1	[{"added": {}}]	6	16
34	2020-03-09 07:19:42.889252+00	5	Sweet Potato	1	[{"added": {}}]	6	16
35	2020-03-09 07:19:55.285962+00	6	Carrots	1	[{"added": {}}]	6	16
36	2020-03-09 07:20:10.97051+00	7	Butternuts	1	[{"added": {}}]	6	16
37	2020-03-09 07:20:58.023872+00	8	Tomato	1	[{"added": {}}]	6	16
38	2020-03-09 07:21:12.265201+00	9	Green Pepper	1	[{"added": {}}]	6	16
39	2020-03-09 07:21:24.727423+00	10	Yellow Pepper	1	[{"added": {}}]	6	16
40	2020-03-09 07:21:36.75519+00	11	Red pepper	1	[{"added": {}}]	6	16
41	2020-03-09 07:21:51.081732+00	12	Cucumber	1	[{"added": {}}]	6	16
42	2020-03-09 07:22:13.23759+00	13	English Cucumber	1	[{"added": {}}]	6	16
43	2020-03-09 07:22:59.023466+00	14	Onion	1	[{"added": {}}]	6	16
44	2020-03-09 07:23:15.998239+00	15	Shallots	1	[{"added": {}}]	6	16
45	2020-03-09 07:23:38.238452+00	16	Garlic	1	[{"added": {}}]	6	16
46	2020-03-09 07:23:50.242655+00	17	Ginger	1	[{"added": {}}]	6	16
47	2020-03-09 07:24:19.478512+00	18	Brocolli	1	[{"added": {}}]	6	16
48	2020-03-09 07:25:10.538342+00	19	Cauliflower	1	[{"added": {}}]	6	16
49	2020-03-09 07:25:45.471193+00	20	Lettuce	1	[{"added": {}}]	6	16
50	2020-03-09 07:26:55.778026+00	21	Cabbage	1	[{"added": {}}]	6	16
51	2020-03-09 07:27:19.797029+00	22	Green Beans	1	[{"added": {}}]	6	16
52	2020-03-09 07:27:36.240339+00	23	Dried Beans	1	[{"added": {}}]	6	16
53	2020-03-09 07:27:56.679174+00	24	Peas	1	[{"added": {}}]	6	16
54	2020-03-09 07:28:12.989263+00	25	Okra	1	[{"added": {}}]	6	16
55	2020-03-09 07:28:38.245081+00	26	Baby Marrow	1	[{"added": {}}]	6	16
56	2020-03-09 07:29:11.279395+00	27	Green Mealies	1	[{"added": {}}]	6	16
57	2020-03-09 07:29:25.043985+00	28	Water Melon\\	1	[{"added": {}}]	6	16
58	2020-03-09 07:29:36.360523+00	28	Water Melon	2	[{"changed": {"fields": ["crop_name"]}}]	6	16
59	2020-03-09 07:29:55.856473+00	29	Banana	1	[{"added": {}}]	6	16
60	2020-03-09 07:30:09.954495+00	30	Mango	1	[{"added": {}}]	6	16
61	2020-03-09 07:30:20.769605+00	31	Orange	1	[{"added": {}}]	6	16
62	2020-03-09 07:30:27.243172+00	32	Lemon	1	[{"added": {}}]	6	16
63	2020-03-09 07:30:38.375155+00	33	Apple	1	[{"added": {}}]	6	16
64	2020-03-09 07:30:50.424882+00	34	Peach	1	[{"added": {}}]	6	16
65	2020-03-09 07:31:04.028363+00	35	PineApple	1	[{"added": {}}]	6	16
66	2020-03-09 07:31:30.295636+00	36	Marcademia	1	[{"added": {}}]	6	16
67	2020-03-09 07:31:44.780609+00	37	Groundnut	1	[{"added": {}}]	6	16
68	2020-03-09 07:32:06.501367+00	38	Roundnut	1	[{"added": {}}]	6	16
69	2020-03-09 07:32:15.869356+00	39	Cowpeas	1	[{"added": {}}]	6	16
70	2020-03-09 07:32:31.210044+00	40	Rapoko	1	[{"added": {}}]	6	16
71	2020-03-09 07:32:40.301729+00	41	Sorghum	1	[{"added": {}}]	6	16
72	2020-03-09 07:32:53.624641+00	42	Tobacco	1	[{"added": {}}]	6	16
73	2020-03-09 07:33:05.052299+00	43	Cotton	1	[{"added": {}}]	6	16
74	2020-03-09 07:34:19.361305+00	3	Fish	1	[{"added": {}}]	9	16
75	2020-03-09 07:34:34.754894+00	4	Broiler	1	[{"added": {}}]	9	16
76	2020-03-09 07:34:52.380803+00	5	Roadrunner	1	[{"added": {}}]	9	16
77	2020-03-09 07:35:07.897788+00	6	Eggs	1	[{"added": {}}]	9	16
78	2020-03-09 07:35:20.366802+00	7	Pig	1	[{"added": {}}]	9	16
79	2020-03-09 07:35:28.578741+00	8	Goat	1	[{"added": {}}]	9	16
80	2020-03-09 07:35:39.91454+00	9	Sheep	1	[{"added": {}}]	9	16
81	2020-03-09 07:35:54.275492+00	10	Guinea Fowl	1	[{"added": {}}]	9	16
82	2020-03-09 07:36:05.70757+00	11	Turkey	1	[{"added": {}}]	9	16
83	2020-03-09 07:36:41.189753+00	12	Cattle	1	[{"added": {}}]	9	16
84	2020-03-09 07:36:49.396167+00	13	Dairy	1	[{"added": {}}]	9	16
85	2020-03-09 07:39:48.849602+00	44	Soya Bean	1	[{"added": {}}]	6	16
86	2020-03-09 07:42:05.8145+00	45	Roses	1	[{"added": {}}]	6	16
87	2020-03-09 07:42:18.57653+00	46	Cutflowers	1	[{"added": {}}]	6	16
88	2020-03-09 18:15:41.149264+00	47	Dried Vegetables	1	[{"added": {}}]	6	16
89	2020-03-09 18:16:58.227426+00	48	Sugarcane	1	[{"added": {}}]	6	16
90	2020-03-09 18:17:11.4781+00	49	Pumpkin	1	[{"added": {}}]	6	16
91	2020-03-09 18:18:03.73004+00	50	Sunflower	1	[{"added": {}}]	6	16
92	2020-05-15 09:31:31.044366+00	2	Takunda Chikondo	3		7	1
93	2020-06-08 16:23:24.976596+00	7	Itai Masuka	1	[{"added": {}}]	8	1
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres_admin
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 93, true);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: postgres_admin
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	contenttypes	contenttype
5	sessions	session
6	agrifriend	crop
7	agrifriend	cropproduction
8	agrifriend	profile
9	agrifriend	animal
10	agrifriend	animalproduction
11	agrifriend	vendor_profile
12	useraccounts	user
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres_admin
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 12, true);


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: postgres_admin
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2020-02-25 11:10:07.809629+00
2	contenttypes	0002_remove_content_type_name	2020-02-25 11:10:07.819317+00
3	auth	0001_initial	2020-02-25 11:10:07.851192+00
4	auth	0002_alter_permission_name_max_length	2020-02-25 11:10:07.883664+00
5	auth	0003_alter_user_email_max_length	2020-02-25 11:10:07.890534+00
6	auth	0004_alter_user_username_opts	2020-02-25 11:10:07.897164+00
7	auth	0005_alter_user_last_login_null	2020-02-25 11:10:07.90594+00
8	auth	0006_require_contenttypes_0002	2020-02-25 11:10:07.908543+00
9	auth	0007_alter_validators_add_error_messages	2020-02-25 11:10:07.915502+00
10	auth	0008_alter_user_username_max_length	2020-02-25 11:10:07.921586+00
11	auth	0009_alter_user_last_name_max_length	2020-02-25 11:10:07.928144+00
12	auth	0010_alter_group_name_max_length	2020-02-25 11:10:07.934486+00
13	auth	0011_update_proxy_permissions	2020-02-25 11:10:07.941165+00
14	useraccounts	0001_initial	2020-02-25 11:10:07.973483+00
15	admin	0001_initial	2020-02-25 11:10:08.020466+00
16	admin	0002_logentry_remove_auto_add	2020-02-25 11:10:08.042254+00
17	admin	0003_logentry_add_action_flag_choices	2020-02-25 11:10:08.054624+00
18	agrifriend	0001_initial	2020-02-25 11:10:08.096028+00
19	agrifriend	0002_auto_20200212_2247	2020-02-25 11:10:08.108444+00
20	agrifriend	0003_crops_produced	2020-02-25 11:10:08.116366+00
21	agrifriend	0004_auto_20200213_2226	2020-02-25 11:10:08.138008+00
22	agrifriend	0005_auto_20200213_2229	2020-02-25 11:10:08.178854+00
23	agrifriend	0006_cropschedule_is_produced	2020-02-25 11:10:08.19215+00
24	agrifriend	0007_auto_20200217_1220	2020-02-25 11:10:08.231606+00
25	agrifriend	0008_animalproctuction	2020-02-25 11:10:08.247494+00
26	agrifriend	0009_animaltype	2020-02-25 11:10:08.258436+00
27	agrifriend	0010_auto_20200218_1325	2020-02-25 11:10:08.263594+00
28	agrifriend	0011_auto_20200218_1445	2020-02-25 11:10:08.301029+00
29	agrifriend	0012_auto_20200218_1619	2020-02-25 11:10:08.310175+00
30	sessions	0001_initial	2020-02-25 11:10:08.320075+00
31	useraccounts	0002_auto_20191202_1009	2020-02-25 11:10:08.368716+00
32	agrifriend	0013_profile_location	2020-03-11 21:22:41.754697+00
33	agrifriend	0014_auto_20200229_0050	2020-03-11 21:22:41.775305+00
34	agrifriend	0015_auto_20200229_0150	2020-03-11 21:22:41.786211+00
35	agrifriend	0016_auto_20200229_0152	2020-03-11 21:22:41.796945+00
36	agrifriend	0017_auto_20200229_0157	2020-03-11 21:22:41.808691+00
37	agrifriend	0018_auto_20200229_0158	2020-03-11 21:22:41.819643+00
38	agrifriend	0019_auto_20200229_0200	2020-03-11 21:22:41.831964+00
39	agrifriend	0020_auto_20200229_0201	2020-03-11 21:22:41.842605+00
40	agrifriend	0021_auto_20200229_0217	2020-03-11 21:22:41.853414+00
41	agrifriend	0022_auto_20200229_1902	2020-03-11 21:22:41.863995+00
42	agrifriend	0023_auto_20200229_1903	2020-03-11 21:22:41.87475+00
43	agrifriend	0024_auto_20200521_2033	2020-05-21 20:03:44.029079+00
44	agrifriend	0025_auto_20200521_2105	2020-05-21 20:03:44.131891+00
45	agrifriend	0026_profile_farming_venture	2020-05-21 20:03:44.176403+00
46	agrifriend	0027_profile_mechanization_level	2020-05-21 20:03:44.229444+00
47	agrifriend	0028_auto_20200521_2127	2020-05-21 20:03:44.292282+00
48	agrifriend	0029_profile_source_of_irrigation_water	2020-05-21 20:03:44.33738+00
49	agrifriend	0030_auto_20200521_2140	2020-05-21 20:03:44.465184+00
50	agrifriend	0031_auto_20200525_2342	2020-05-25 22:10:27.797292+00
51	agrifriend	0032_auto_20200525_2345	2020-05-25 22:10:27.810081+00
52	agrifriend	0033_auto_20200525_2348	2020-05-25 22:10:27.820842+00
53	agrifriend	0034_auto_20200525_2349	2020-05-25 22:10:27.831667+00
54	agrifriend	0035_auto_20200525_2354	2020-05-25 22:10:27.842455+00
55	agrifriend	0037_auto_20200620_0802	2020-06-20 14:06:27.010841+00
56	agrifriend	0038_auto_20200620_0812	2020-06-20 14:06:27.026454+00
57	agrifriend	0039_auto_20200620_0831	2020-06-20 14:06:27.038225+00
58	agrifriend	0040_auto_20200620_0955	2020-06-20 14:06:27.0543+00
59	agrifriend	0041_auto_20200620_1611	2020-06-20 14:11:43.835458+00
\.


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres_admin
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 59, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: postgres_admin
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
edsic8duyf8ttxirrjxo3gmg6flc9azr	YWE4MWM4NzMwMzMwNzBlZmUxYmExYWU3OGE5ZmVmMmRlMWY2ZDA0OTp7Il9hdXRoX3VzZXJfaGFzaCI6IjAyNDYxZTFiZGNjMzIzODdmNThhNGY0NjM3NmZiM2ZkNTU2M2JjMTQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIzMSJ9	2020-03-26 12:48:31.259478+00
9449xq68f2p00zx5u44edujgwjsh2jhr	ZjQzNDZkMTRiOGFmZTJhNzA1MGU2YjdmN2Q5OTA1NWEyZjYyMzYxNzp7Il9hdXRoX3VzZXJfaGFzaCI6IjdkZDJiZGU0OTI5YjZiNDBiMjQzY2I4Yjc5MzYxZjRlY2RhNzM3OWEiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2020-03-11 22:15:50.882475+00
f8wtic7tf9ldgklr6gqrj8u6cwl37d0q	Njk5YmVhZGM5YjY4NjU5ZGRhYTYzNmYyMWQyOWY3MjBmZWI5YWQ2ZTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYTY4M2NmMzc1Mjg3NDFiNzY3OTI0NzMxODIyMmRmOTEyODk3ZjIzYiIsIl9hdXRoX3VzZXJfaWQiOiIxNiJ9	2020-04-11 15:12:23.821895+00
to7ercx8kz4n12uvg77d76q8io6tsagf	NjA5NDdmNjA1Njk3OTk2YjJhN2JhMWNlMTc4YjQxNWY4MzYyZDdkZTp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2RkMmJkZTQ5MjliNmI0MGIyNDNjYjhiNzkzNjFmNGVjZGE3Mzc5YSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2020-03-12 01:04:30.102327+00
zbsqfa2091vtz8cqdzxjt48kthmsjt4g	M2FmNDk3ZDZjYmU3ODkwNjBmY2UxNGNhOTZiODhjNmZlOGU1YWQyNDp7Il9hdXRoX3VzZXJfaWQiOiIxNiIsIl9hdXRoX3VzZXJfaGFzaCI6ImE2ODNjZjM3NTI4NzQxYjc2NzkyNDczMTgyMjJkZjkxMjg5N2YyM2IiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2020-04-24 11:33:27.47912+00
60ay00z0grx8p1wobffdx92dh7kqghae	M2FmNDk3ZDZjYmU3ODkwNjBmY2UxNGNhOTZiODhjNmZlOGU1YWQyNDp7Il9hdXRoX3VzZXJfaWQiOiIxNiIsIl9hdXRoX3VzZXJfaGFzaCI6ImE2ODNjZjM3NTI4NzQxYjc2NzkyNDczMTgyMjJkZjkxMjg5N2YyM2IiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2020-04-27 17:12:42.216797+00
ybkej9yj0doepbu0w7dy0jv33cen09wv	OTNkZGU1ODM0NTMyYTg2MWM1MmI1NzZjYjAwOTg4ZjhlMDA3OWE3Nzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE2IiwiX2F1dGhfdXNlcl9oYXNoIjoiYTY4M2NmMzc1Mjg3NDFiNzY3OTI0NzMxODIyMmRmOTEyODk3ZjIzYiJ9	2020-03-12 07:35:42.958949+00
n6aeawcr8nqef08b2tnawu51truof952	OTNkZGU1ODM0NTMyYTg2MWM1MmI1NzZjYjAwOTg4ZjhlMDA3OWE3Nzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE2IiwiX2F1dGhfdXNlcl9oYXNoIjoiYTY4M2NmMzc1Mjg3NDFiNzY3OTI0NzMxODIyMmRmOTEyODk3ZjIzYiJ9	2020-03-12 07:35:46.545477+00
st76mm8vlfvon576oaba8398929bdyr5	M2FmNDk3ZDZjYmU3ODkwNjBmY2UxNGNhOTZiODhjNmZlOGU1YWQyNDp7Il9hdXRoX3VzZXJfaWQiOiIxNiIsIl9hdXRoX3VzZXJfaGFzaCI6ImE2ODNjZjM3NTI4NzQxYjc2NzkyNDczMTgyMjJkZjkxMjg5N2YyM2IiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2020-05-12 09:37:04.968807+00
omhhrm0l93fg1k9olaw2pz6b8rsawrgi	YzUwZjhhYjI1MzBhODA5Njc0M2E5ZGQ4OTQxNDJmZjAxMTZiNzE1YTp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiN2RkMmJkZTQ5MjliNmI0MGIyNDNjYjhiNzkzNjFmNGVjZGE3Mzc5YSIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=	2020-05-25 17:27:59.346247+00
ihudqp7nb0y67wz2lmby54vphtdx52lh	NzNiNTAzZTNiOWUyMmY2ZDQ5ODA0NjcyZDM5MmJiNTMyMjZjNWE1Mzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiI3ZGQyYmRlNDkyOWI2YjQwYjI0M2NiOGI3OTM2MWY0ZWNkYTczNzlhIn0=	2020-05-31 11:00:35.309435+00
qv4vkcwlwry2vb28yw945073fy0efhg5	MDIzNjY3MGNjN2RhZGM4NDg2NzY1YWM3ZGRiZWIxMjRiNTE1ODRhZTp7Il9hdXRoX3VzZXJfaWQiOiIxNiIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9oYXNoIjoiYTY4M2NmMzc1Mjg3NDFiNzY3OTI0NzMxODIyMmRmOTEyODk3ZjIzYiJ9	2020-05-31 11:18:23.236416+00
3o5bmns8vhcr28cnx9ecofzys47us80b	YTllNmRmYTdmNDA3N2U4YWM4MTRjZDMwODI2NDUxODg2MzE0M2IxNjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjEiLCJfYXV0aF91c2VyX2hhc2giOiI3ZGQyYmRlNDkyOWI2YjQwYjI0M2NiOGI3OTM2MWY0ZWNkYTczNzlhIn0=	2020-06-04 20:05:17.562507+00
v3gws5cpiku0cijp1qszmyqggak0j7yj	MDQ4NDEzNDE1M2I2ODI1ZDQ3N2YyMzg1OGQ4ZTI3MWU5NDI4YTU1Nzp7fQ==	2020-06-15 14:26:55.458946+00
h0v246ngl6pa79yqfnybomma0sm44lfj	MzQ5YzkxZTdkZTJhNDNjZGRiMjZiMGQwNzhhNTViM2M3NTU0MGUxYzp7Il9hdXRoX3VzZXJfaGFzaCI6ImNhZTdmMTQzYmQ3ZjMxYzE4MGJkMjA5NTI2ZTlkMDQxNmZkNmI5ZmYiLCJfYXV0aF91c2VyX2lkIjoiMzYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2020-06-16 12:24:57.242984+00
72vxhyc3thzjvrqdo0vgg36vaty4ttcb	MDQ4NDEzNDE1M2I2ODI1ZDQ3N2YyMzg1OGQ4ZTI3MWU5NDI4YTU1Nzp7fQ==	2020-06-22 14:43:51.984624+00
8mou070q9h3ojun05cz0ydztb4d3wwio	MDQ4NDEzNDE1M2I2ODI1ZDQ3N2YyMzg1OGQ4ZTI3MWU5NDI4YTU1Nzp7fQ==	2020-06-22 14:44:26.300507+00
g0cz4ksjqt9qma8sfszc63nq01dd6qsi	ZjQzNDZkMTRiOGFmZTJhNzA1MGU2YjdmN2Q5OTA1NWEyZjYyMzYxNzp7Il9hdXRoX3VzZXJfaGFzaCI6IjdkZDJiZGU0OTI5YjZiNDBiMjQzY2I4Yjc5MzYxZjRlY2RhNzM3OWEiLCJfYXV0aF91c2VyX2lkIjoiMSIsIl9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIn0=	2020-06-22 16:05:19.512577+00
6lwmyzturujh8cna5dkwh1x6r5qiwfk2	MDQ4NDEzNDE1M2I2ODI1ZDQ3N2YyMzg1OGQ4ZTI3MWU5NDI4YTU1Nzp7fQ==	2020-06-22 16:17:32.429246+00
tyjwxn55a2lfn3zdil5zk3gp8z770nar	MDQ4NDEzNDE1M2I2ODI1ZDQ3N2YyMzg1OGQ4ZTI3MWU5NDI4YTU1Nzp7fQ==	2020-06-22 16:18:30.21779+00
cv90ryqlccr6kt9490nxrsdxebqpmoww	MDQ4NDEzNDE1M2I2ODI1ZDQ3N2YyMzg1OGQ4ZTI3MWU5NDI4YTU1Nzp7fQ==	2020-06-22 16:20:42.41437+00
jp1upg1mp11cff8890m2aledut6pqoag	MDQ4NDEzNDE1M2I2ODI1ZDQ3N2YyMzg1OGQ4ZTI3MWU5NDI4YTU1Nzp7fQ==	2020-06-22 16:20:53.503772+00
u8jloabn5vldrg5pv40qp8etdx5959ul	ZTQ3MGY0NzEyNWYyMDg4N2QyNzhhZTY3MGJjY2U1NDI2ZDQzMTA5ZDp7Il9hdXRoX3VzZXJfaGFzaCI6ImE2ODNjZjM3NTI4NzQxYjc2NzkyNDczMTgyMjJkZjkxMjg5N2YyM2IiLCJfYXV0aF91c2VyX2lkIjoiMTYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9	2020-06-22 16:23:52.145534+00
pzosbt3j3y19pbqq0gzebffzch66mlvv	MDQ4NDEzNDE1M2I2ODI1ZDQ3N2YyMzg1OGQ4ZTI3MWU5NDI4YTU1Nzp7fQ==	2020-07-04 08:26:09.786468+00
kwgd1lcr5p5fs9lde241ndgfhh2s18bz	MDQ4NDEzNDE1M2I2ODI1ZDQ3N2YyMzg1OGQ4ZTI3MWU5NDI4YTU1Nzp7fQ==	2020-07-04 08:29:11.301823+00
53he0ci11mmdtoxul4me7jbz326u8j4l	NWRhYWEyYTc4OWYyMzMxNTYwZjllMjA4NTFmZTAxZGZmNTUxNjU0Mjp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjQxIiwiX2F1dGhfdXNlcl9oYXNoIjoiMGQ3YzM4YjE0YzJmMTVhMTI4ZjkxN2E3Mzk4Y2RjZDY0NjYwN2RlNCJ9	2020-07-06 18:27:45.367721+00
ltqgxguxqg59jd92odkbv0quyvqiz0fi	OTNkZGU1ODM0NTMyYTg2MWM1MmI1NzZjYjAwOTg4ZjhlMDA3OWE3Nzp7Il9hdXRoX3VzZXJfYmFja2VuZCI6ImRqYW5nby5jb250cmliLmF1dGguYmFja2VuZHMuTW9kZWxCYWNrZW5kIiwiX2F1dGhfdXNlcl9pZCI6IjE2IiwiX2F1dGhfdXNlcl9oYXNoIjoiYTY4M2NmMzc1Mjg3NDFiNzY3OTI0NzMxODIyMmRmOTEyODk3ZjIzYiJ9	2020-07-08 14:40:57.89153+00
\.


--
-- Data for Name: useraccounts_user; Type: TABLE DATA; Schema: public; Owner: postgres_admin
--

COPY public.useraccounts_user (id, password, last_login, is_superuser, first_name, last_name, is_staff, is_active, date_joined, email, is_farmer, is_vendor) FROM stdin;
33	pbkdf2_sha256$150000$NjNL7GcS4jVH$gjKPuPjOVx0T0sBx+tCyRdcnPHhXQV8rO1Z+gxVE4Og=	\N	f			f	f	2020-04-10 11:31:55.248223+00	j2@gmail.com	f	f
34	pbkdf2_sha256$150000$RhuKDYDqwrrw$TitS8n6xuF64Ve7wGKD5RA4gwfOcDZ9EByLa6ncntpY=	\N	f			f	f	2020-04-10 11:31:58.407691+00	sibandam@yahoo.com	f	f
28	pbkdf2_sha256$150000$o4PpMY7OgLvN$MuMnJ9N9u/V+U9xj20ek+vDlxuxFf/ta9FR6Y72Gs7o=	\N	f			f	f	2020-03-09 07:51:05.535985+00	masukamj@gmail.com	f	f
35	pbkdf2_sha256$150000$XsVarFsGlCxY$mXK0e8XEuvDCU6wNMOjiUp4x1PEPNAxo0BMGiuN4dKY=	\N	f			f	f	2020-04-10 14:47:34.254887+00	clubezim@gmail.com	f	f
37	pbkdf2_sha256$150000$hrw6oXkdmBYm$eT6ie78E4jcXjBhf22HWTOhL7S27bK51tdre8K2a0Os=	\N	f			f	t	2020-06-13 22:12:42.89592+00	munyimani.tapera@gmail.com	f	f
1	pbkdf2_sha256$150000$uyKyRI7wSR0V$Sk9DohszkyKazWa3eKShhmJz5FbluPVrDK/x8yA35hE=	2020-06-20 14:12:12.930296+00	t			t	t	2020-02-25 17:56:42.491427+00	tkchikondo@gmail.com	f	f
29	pbkdf2_sha256$150000$gPpDOOf2321I$48ZseVLjt6QT/3IYSeuSbG1OnktxXfsy/gcYC/e5++s=	\N	f			f	f	2020-03-12 06:57:16.324806+00	swishingchibs@live.com	f	f
27	pbkdf2_sha256$150000$bmSnuJ5D3WdW$uoDYfpoS3cmgGXxpkQ2QoWKHb/TVbjDYBUOgVfa6aIs=	2020-03-12 06:59:43.422159+00	f			f	t	2020-03-04 16:22:26.98603+00	noble.masuka@gmail.com	f	f
38	pbkdf2_sha256$150000$mJeYuCcsLtpE$+ZqIQWMKKbSiOVXWuM0Urh7nD8+8Z2pOH7ZFj7EnOh0=	\N	f			f	t	2020-06-21 17:12:56.762733+00	jchineka@yahoo.com	f	f
30	pbkdf2_sha256$150000$c4oPVHvL0eWj$LbkfZGLwypj92fLARVhOTAklTAt5iL6wTMiXRRdm8Ew=	2020-03-12 07:17:34.613545+00	f			f	t	2020-03-12 07:13:33.878152+00	swishingchibs@gmail.com	f	f
39	pbkdf2_sha256$150000$EsIFfEG0vJNQ$phmeDXPYDYWOphAoQbi01xQMVYN6puSs/rsHIxvEVs4=	\N	f			f	t	2020-06-22 10:47:37.713457+00	clanyemashx@gmail.com	f	f
40	pbkdf2_sha256$150000$n9crcDknrvob$2TNb6wYWrEu52S/sd81IEyv7yiEn6oVgiK/RWrMgOu0=	\N	f			f	t	2020-06-22 17:37:07.73927+00	masiiwak@gmail.com	f	f
41	pbkdf2_sha256$150000$SbqY5s37KthE$4yp+Vs533HzsiQss3Vj54fdDIAiAHGUK8A1TpiWN8to=	2020-06-22 18:27:45.361572+00	f			f	t	2020-06-22 18:26:54.926508+00	rachel@kanjani.biz	f	f
31	pbkdf2_sha256$150000$k16DdKPfYX0N$TVEr96K1U330IS/3VA3MfVN4oPYhYk8AfHmbhqntdAk=	2020-03-12 17:19:17.789738+00	f			f	t	2020-03-12 12:04:31.917575+00	mmasuka@timb.co.zw	f	f
16	pbkdf2_sha256$150000$dMkGPnceJ11Y$n21IZWjOI4YhITnFr3d/HRYJtZG0zRTgedsvwaf3QWM=	2020-06-24 14:40:57.885816+00	t	Itai	Masuka	t	t	2020-02-26 22:17:30+00	engmasuka@ulitmeaccolade.co.zw	f	f
32	pbkdf2_sha256$150000$ndEc3u0BN5FS$Sk482Yt93BI0TDXaseeHl9FC4DdokvRpZfI7mz0l96o=	\N	f			f	f	2020-03-23 19:30:46.306543+00	rgchinhamo@gmail.com	f	f
36	pbkdf2_sha256$150000$PisywvQVM3mK$7vQvEz01P8sqWOxvI0IH4UI3yNWc2obwmBW4bIcLGdY=	2020-06-02 12:24:57.23807+00	f			f	t	2020-06-02 12:16:01.699848+00	ccmasuka@gmail.com	f	f
42	pbkdf2_sha256$150000$yevIZ2EEexWX$eMIJJqiVw2JSIEEdoIN5OAvY9U+FbRqey0jbf7x8XoM=	\N	f			f	t	2020-06-27 06:26:19.977337+00	samudawapi@gmail.com	f	f
43	pbkdf2_sha256$150000$ip3CWMRQBsVI$HOpImITLZsAVleqiPzmbQDtVKUTypzGDv8hJ52PdSaI=	2020-07-12 15:17:14.986902+00	f			f	t	2020-07-12 14:56:14.989086+00	chizhandec@gmail.com	f	f
\.


--
-- Data for Name: useraccounts_user_groups; Type: TABLE DATA; Schema: public; Owner: postgres_admin
--

COPY public.useraccounts_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Name: useraccounts_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres_admin
--

SELECT pg_catalog.setval('public.useraccounts_user_groups_id_seq', 1, false);


--
-- Name: useraccounts_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres_admin
--

SELECT pg_catalog.setval('public.useraccounts_user_id_seq', 43, true);


--
-- Data for Name: useraccounts_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: postgres_admin
--

COPY public.useraccounts_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Name: useraccounts_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres_admin
--

SELECT pg_catalog.setval('public.useraccounts_user_user_permissions_id_seq', 1, false);


--
-- Name: agrifriend_animal_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.agrifriend_animal
    ADD CONSTRAINT agrifriend_animal_pkey PRIMARY KEY (id);


--
-- Name: agrifriend_animalproduction_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.agrifriend_animalproduction
    ADD CONSTRAINT agrifriend_animalproduction_pkey PRIMARY KEY (id);


--
-- Name: agrifriend_crop_schedule_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.agrifriend_cropproduction
    ADD CONSTRAINT agrifriend_crop_schedule_pkey PRIMARY KEY (id);


--
-- Name: agrifriend_crop_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.agrifriend_crop
    ADD CONSTRAINT agrifriend_crop_type_pkey PRIMARY KEY (id);


--
-- Name: agrifriend_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.agrifriend_profile
    ADD CONSTRAINT agrifriend_profile_pkey PRIMARY KEY (id);


--
-- Name: agrifriend_profile_user_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.agrifriend_profile
    ADD CONSTRAINT agrifriend_profile_user_id_key UNIQUE (user_id);


--
-- Name: agrifriend_vendor_profile_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.agrifriend_vendor_profile
    ADD CONSTRAINT agrifriend_vendor_profile_pkey PRIMARY KEY (id);


--
-- Name: agrifriend_vendor_profile_user_id_key; Type: CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.agrifriend_vendor_profile
    ADD CONSTRAINT agrifriend_vendor_profile_user_id_key UNIQUE (user_id);


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: useraccounts_user_email_key; Type: CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.useraccounts_user
    ADD CONSTRAINT useraccounts_user_email_key UNIQUE (email);


--
-- Name: useraccounts_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.useraccounts_user_groups
    ADD CONSTRAINT useraccounts_user_groups_pkey PRIMARY KEY (id);


--
-- Name: useraccounts_user_groups_user_id_group_id_53432322_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.useraccounts_user_groups
    ADD CONSTRAINT useraccounts_user_groups_user_id_group_id_53432322_uniq UNIQUE (user_id, group_id);


--
-- Name: useraccounts_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.useraccounts_user
    ADD CONSTRAINT useraccounts_user_pkey PRIMARY KEY (id);


--
-- Name: useraccounts_user_user_p_user_id_permission_id_c40262f5_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.useraccounts_user_user_permissions
    ADD CONSTRAINT useraccounts_user_user_p_user_id_permission_id_c40262f5_uniq UNIQUE (user_id, permission_id);


--
-- Name: useraccounts_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.useraccounts_user_user_permissions
    ADD CONSTRAINT useraccounts_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: agrifriend_animalproduction_farmer_id_e4599728; Type: INDEX; Schema: public; Owner: postgres_admin
--

CREATE INDEX agrifriend_animalproduction_farmer_id_e4599728 ON public.agrifriend_animalproduction USING btree (farmer_id);


--
-- Name: agrifriend_crop_schedule_farmer_id_4edf38f6; Type: INDEX; Schema: public; Owner: postgres_admin
--

CREATE INDEX agrifriend_crop_schedule_farmer_id_4edf38f6 ON public.agrifriend_cropproduction USING btree (farmer_id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: postgres_admin
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: postgres_admin
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: postgres_admin
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: postgres_admin
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: postgres_admin
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: postgres_admin
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: postgres_admin
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: postgres_admin
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: useraccounts_user_email_274047e7_like; Type: INDEX; Schema: public; Owner: postgres_admin
--

CREATE INDEX useraccounts_user_email_274047e7_like ON public.useraccounts_user USING btree (email varchar_pattern_ops);


--
-- Name: useraccounts_user_groups_group_id_b571efcc; Type: INDEX; Schema: public; Owner: postgres_admin
--

CREATE INDEX useraccounts_user_groups_group_id_b571efcc ON public.useraccounts_user_groups USING btree (group_id);


--
-- Name: useraccounts_user_groups_user_id_455d2154; Type: INDEX; Schema: public; Owner: postgres_admin
--

CREATE INDEX useraccounts_user_groups_user_id_455d2154 ON public.useraccounts_user_groups USING btree (user_id);


--
-- Name: useraccounts_user_user_permissions_permission_id_5ed6d9a1; Type: INDEX; Schema: public; Owner: postgres_admin
--

CREATE INDEX useraccounts_user_user_permissions_permission_id_5ed6d9a1 ON public.useraccounts_user_user_permissions USING btree (permission_id);


--
-- Name: useraccounts_user_user_permissions_user_id_21ab68b2; Type: INDEX; Schema: public; Owner: postgres_admin
--

CREATE INDEX useraccounts_user_user_permissions_user_id_21ab68b2 ON public.useraccounts_user_user_permissions USING btree (user_id);


--
-- Name: agrifriend_animalpro_farmer_id_e4599728_fk_agrifrien; Type: FK CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.agrifriend_animalproduction
    ADD CONSTRAINT agrifriend_animalpro_farmer_id_e4599728_fk_agrifrien FOREIGN KEY (farmer_id) REFERENCES public.agrifriend_profile(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agrifriend_cropprodu_farmer_id_a6db8bf6_fk_agrifrien; Type: FK CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.agrifriend_cropproduction
    ADD CONSTRAINT agrifriend_cropprodu_farmer_id_a6db8bf6_fk_agrifrien FOREIGN KEY (farmer_id) REFERENCES public.agrifriend_profile(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agrifriend_profile_user_id_b004a6ce_fk_useraccounts_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.agrifriend_profile
    ADD CONSTRAINT agrifriend_profile_user_id_b004a6ce_fk_useraccounts_user_id FOREIGN KEY (user_id) REFERENCES public.useraccounts_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: agrifriend_vendor_pr_user_id_dcc6868c_fk_useraccou; Type: FK CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.agrifriend_vendor_profile
    ADD CONSTRAINT agrifriend_vendor_pr_user_id_dcc6868c_fk_useraccou FOREIGN KEY (user_id) REFERENCES public.useraccounts_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_c564eba6_fk_useraccounts_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_useraccounts_user_id FOREIGN KEY (user_id) REFERENCES public.useraccounts_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: useraccounts_user_gr_user_id_455d2154_fk_useraccou; Type: FK CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.useraccounts_user_groups
    ADD CONSTRAINT useraccounts_user_gr_user_id_455d2154_fk_useraccou FOREIGN KEY (user_id) REFERENCES public.useraccounts_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: useraccounts_user_groups_group_id_b571efcc_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.useraccounts_user_groups
    ADD CONSTRAINT useraccounts_user_groups_group_id_b571efcc_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: useraccounts_user_us_permission_id_5ed6d9a1_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.useraccounts_user_user_permissions
    ADD CONSTRAINT useraccounts_user_us_permission_id_5ed6d9a1_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: useraccounts_user_us_user_id_21ab68b2_fk_useraccou; Type: FK CONSTRAINT; Schema: public; Owner: postgres_admin
--

ALTER TABLE ONLY public.useraccounts_user_user_permissions
    ADD CONSTRAINT useraccounts_user_us_user_id_21ab68b2_fk_useraccou FOREIGN KEY (user_id) REFERENCES public.useraccounts_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

