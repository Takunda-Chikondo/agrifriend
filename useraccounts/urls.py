from django.contrib import admin
from django.views.generic.base import RedirectView
from django.urls import path, include, reverse_lazy
from django.conf.urls import url
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    path('signup/', views.FarmerSignUpView.as_view(), name='usersignup'),
    path('login/', auth_views.LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(), name='logout'),
    path('create_profile/', views.ProfileCreateView.as_view(), name='create_profile'),
    path('profile/', views.ProfileRedirectView.as_view(), name='profile_redirect'),
    path('profile-update/<int:pk>/', views.ProfileUpdateView.as_view(), name='update_profile'),
    path('activate/<slug:uidb64>/<slug:token>/',views.activate_account, name='activate'),

    # password management urls
    path('password_change/',
    auth_views.PasswordChangeView.as_view(template_name='password_change.html'),
    name='password_change'
    ),

    path('password_change/done/',
        auth_views.PasswordChangeDoneView.as_view(template_name='password_change_done.html'),
        name='password_change_done'
    ),

    path('password_reset/',
        auth_views.PasswordResetView.as_view(
            template_name='password_reset_form.html',
            email_template_name='password_reset_email.html',
            success_url=reverse_lazy('password_reset_done')
        ),
        name='password_reset'
    ),
    path('password_reset/done/',
        auth_views.PasswordResetDoneView.as_view(
            template_name='password_reset_done.html'),
        name='password_reset_done'
    ),
    path('reset/<uidb64>/<token>/',
        auth_views.PasswordResetConfirmView.as_view(
            template_name='password_reset_confirm.html'),
        name='password_reset_confirm'
    ),
    path('reset/done/',
        auth_views.PasswordResetCompleteView.as_view(
            template_name='password_reset_complete.html'), name='password_reset_complete'),
]
