from django import forms
from django.forms import inlineformset_factory
from agrifriend.models import Profile, Vendor_profile, CropProduction, Crop, AnimalProduction, Animal
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model
from django.db import transaction

User = get_user_model()

class FarmerSignUpForm(UserCreationForm):
    email = forms.EmailField(max_length=100, help_text='Required')
    first_name = forms.CharField(max_length=200)
    last_name = forms.CharField(max_length=200)
    phone_number = forms.CharField(max_length=200)
    nearest_growth_point = forms.CharField(max_length=200)
    farm_size = forms.IntegerField()

    class Meta(UserCreationForm.Meta):
        model = User
        fields = ('email', 'password1', 'password2')

class CropScheduleForm(forms.ModelForm):
    crop_type = forms.ModelChoiceField(queryset=Crop.objects.all(),to_field_name="crop_name",empty_label="select")

    class Meta:
        model = CropProduction
        fields = '__all__'
        labels = {
            "quantity_per_week" : "Quantity/week"
        }

CropScheduleFormSet = inlineformset_factory(Profile, CropProduction, form=CropScheduleForm, extra=1)

class AnimalProctuctionForm(forms.ModelForm):
    animal_type = forms.ModelChoiceField(queryset=Animal.objects.all(),to_field_name="animal_name",empty_label="select")

    class Meta:
        model = AnimalProduction
        fields = '__all__'
        labels = {
            "quantity_per_week" : "Quantity/week"
        }

AnimalProductionFormset = inlineformset_factory(Profile, AnimalProduction, form=AnimalProctuctionForm, extra=1)
