from django.contrib.auth import get_user_model
from django.test import TestCase
from django.db import models
from agrifriend.models import Profile
import pdb;

class TestProfileModel(TestCase):

    def test_profile_creation(self):
        User = get_user_model()
        # New user created
        user = User.objects.create(email="a@a.com", password="django-tutorial")
        # Check that a Profile instance has been created
        # pdb.set_trace()
        self.assertIsInstance(user.profile, Profile)
        # Call the save method of the user to activate the signal
        # again, and check that it doesn't try to create another
        # profile instace
        user.save()
        self.assertIsInstance(user.profile, Profile)
