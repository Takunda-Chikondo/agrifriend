from django.http import HttpResponse, HttpResponseRedirect
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import send_mail, BadHeaderError
from django.views.generic.base import RedirectView
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, get_user_model
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from .token_generator import account_activation_token
from django.core.mail import EmailMessage
from django.template import loader
from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.urls import reverse_lazy, reverse
from .forms import FarmerSignUpForm, CropScheduleFormSet, AnimalProductionFormset
from django.views.generic import CreateView, UpdateView
from agrifriend.models import Profile
import pdb;

User = get_user_model()

class FarmerSignUpView(CreateView):
    model = User
    form_class = FarmerSignUpForm
    template_name = 'signup.html'

    def get_context_data(self, **kwargs):
        kwargs['user_type'] = 'farmer'
        return super().get_context_data(**kwargs)

    def form_valid(self, form):
        user = form.save()
        user.is_active = False
        user.refresh_from_db()  # load the profile instance created by the signal
        user.profile.first_name = form.cleaned_data.get('first_name')
        user.profile.last_name = form.cleaned_data.get('last_name')
        user.profile.phone_number = form.cleaned_data.get('phone_number')
        user.profile.nearest_growth_point = form.cleaned_data.get('nearest_growth_point')
        user.profile.farm_size = form.cleaned_data.get('farm_size')
        user.save()
        current_site = get_current_site(self.request)
        email_subject = 'Activate Your Account'
        message = render_to_string('activate_account.html', {
            'user': user,
            'domain': current_site.domain,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'token': account_activation_token.make_token(user),
        })
        to_email = form.cleaned_data.get('email')
        email = EmailMessage(email_subject, message, 'noreply@ulitmeaccolade.co.zw', to=[to_email])
        email.send()
        return render(self.request, 'email_confirm.html')

class ProfileCreateView(CreateView):
    template_name = 'profile.html'
    model = Profile
    fields = '__all__'
    success_url = reverse_lazy('index')

    def get_context_data(self, **kwargs):
        data = super(ProfileCreateView, self).get_context_data(**kwargs)
        if self.request.POST:
            data['cropsproduced'] = CropScheduleFormSet(self.request.POST, self.request.FILES)
            data['animalsproduced'] = AnimalProductionFormset(self.request.POST, self.request.FILES)
        else:
            data['cropsproduced'] = CropScheduleFormSet()
            data['animalsproduced'] = AnimalProductionFormset()
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        cropsproduced = context['cropsproduced']
        animalsproduced = context['animalsproduced']
        with transaction.atomic():
            ## add the user to the form before the user has been created
            form.instance.user = self.request.user
            self.object = form.save()
            if cropsproduced.is_valid() and animalsproduced.is_valid():
                cropsproduced.instance = self.object
                cropsproduced.save()

                animalsproduced.instance = self.object
                animalsproduced.save()
        return super(ProfileCreateView, self).form_valid(form)


class ProfileUpdateView(UpdateView):

    model = Profile
    template_name = 'profile.html'
    fields = '__all__'
    success_url = reverse_lazy('index')
    success_message = "Saved successfully"
    error_message = "An error occured"

    def get_context_data(self, **kwargs):
        data = super(ProfileUpdateView, self).get_context_data(**kwargs)
        if self.request.POST:
            data['cropsproduced'] = CropScheduleFormSet(self.request.POST, self.request.FILES,  instance=self.object)
            data['animalsproduced'] = AnimalProductionFormset(self.request.POST, self.request.FILES, instance=self.object)
        else:
            data['cropsproduced'] = CropScheduleFormSet(instance=self.object)
            data['animalsproduced'] = AnimalProductionFormset(instance=self.object)
        return data

    def form_valid(self, form):
        context = self.get_context_data()
        cropsproduced = context['cropsproduced']
        animalsproduced = context['animalsproduced']
        with transaction.atomic():
            self.object = form.save()

            if cropsproduced.is_valid() and animalsproduced.is_valid():
                cropsproduced.instance = self.object
                cropsproduced.save()

                animalsproduced.instance = self.object
                animalsproduced.save()

        return super(ProfileUpdateView, self).form_valid(form)

class ProfileRedirectView(RedirectView):

    def get_redirect_url(self):
        try:
            profile = User.objects.get(pk=self.request.user.id).profile
        except ObjectDoesNotExist:
            profile = None

        if profile is None:
            return reverse('create_profile')
        else:
            return reverse('update_profile', kwargs={'pk': profile.id})

def activate_account(request, uidb64, token):
    try:
        uid = force_bytes(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save()
        login(request)
        context = {}
        template = loader.get_template('account_activated.html')
        return HttpResponse(template.render({'context': context}, request))
    else:
        return HttpResponse('Activation link is invalid!')

def login(request):
    context = {}
    template = loader.get_template('login.html')
    return HttpResponse(template.render({'context': context}, request))
